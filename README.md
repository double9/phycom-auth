### Authentication module for Phycom platform

Contains a collection of 3rd party authentication methods that can be easily added to phycom website.


### List of methods provided:

* Facebook
* Google


### Installation

Install using composer:

```
composer install phycom/auth
```


### How to use the module

1. Add module config your site, see example here:

```
<?php

return  [
    'modules' => [
        'auth' => [
            'class' => \Phycom\Auth\Module::class,
            'methods' => [
                'google' => [
                    'class' => \Phycom\Auth\Methods\Google\Module::class,
                    'clientId' => '<your google client ID here>'
                ],
                'facebook' => [
                    'class' => \Phycom\Auth\Methods\Facebook\Module::class,
                    'appId' => '<your facebook ap id here>'
                ]
            ]
        ]
    ]
];

```

2. Add auth. methods to your login form like this:


```

foreach (Yii::$app->getModule('auth')->getMethods() as $method) {
    echo $method->render();
}

```
