<?php
namespace Phycom\Auth;

use Phycom\Auth\Methods\AuthenticationMethod;
use Phycom\Base\Components\MessageSource;

use Phycom\Base\Interfaces\PhycomComponentInterface;
use Phycom\Base\Modules\BaseModule;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class Module
 * @package Phycom\Auth
 */
class Module extends BaseModule
{
    const METHOD_GOOGLE = 'google';
    const METHOD_FACEBOOK = 'facebook';

    public function init()
    {
        parent::init();

        $this->registerTranslations();

        $subModules = [
            self::METHOD_GOOGLE => [
                'class'       => Methods\Google\Module::class,
                'enabled'     => false,
                'clientId'    => getenv(ENV . '_GOOGLE_CLIENT_ID'),
                'useSettings' => true
            ],
            self::METHOD_FACEBOOK => [
                'class'       => Methods\Facebook\Module::class,
                'enabled'     => false,
                'appId'       => getenv(ENV . '_FB_APP_ID'),
                'useSettings' => true
            ]
        ];
        foreach ($subModules as $id => $moduleParams) {
            $params = isset($this->modules[$id]) ? ArrayHelper::merge($moduleParams, $this->modules[$id]) : $moduleParams;
            $this->setModule($id, $params);
        }
    }

    public function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations['phycom/auth*'])) {
            Yii::$app->i18n->translations['phycom/auth*'] = [
                'class'    => MessageSource::class,
                'basePath' => '@phycom/auth/translations',
                'catalog'  => false
            ];
        }
    }

    public function getLabel() : string
    {
        return Yii::t('phycom/auth', 'Authentication methods');
    }

    public function getAllSettings(): array
    {
        $modules = [];
        foreach ($this->modules as $id => $moduleParams) {
            $modules[] = $this->getModule($id);
        }
        return $modules;
    }

    /**
     * @return AuthenticationMethod[]
     */
    public function getMethods()
    {
        $enabled = [];

        foreach ($this->modules as $id => $moduleParams) {
            $module = $this->getModule($id);
            /**
             * @var AuthenticationMethod $module
             */
            if ($module->getIsEnabled()) {
                $enabled[] = $module;
            }
        }
        return $enabled;
    }

    /**
     * @param string $id - method id
     * @return AuthenticationMethod
     */
    public function getMethod($id)
    {
        /**
         * @var AuthenticationMethod $module
         */
        $module = $this->getModule($id);
        return $module && $module->getIsEnabled() ? $module : null;
    }

    /**
     * @param string $method - method id
     * @param null $label
     * @param array $options
     * @return string
     */
    public function render($method, $label = null, array $options = [])
    {
        $method = $this->getMethod($method);
        return $method ? $method->render($label, $options) : '';
    }
}
