<?php

namespace Phycom\Auth\Exceptions;

use yii\base\Exception;

/**
 * Class AuthException
 * @package Phycom\Auth\Exceptions
 */
class AuthException extends Exception
{

}
