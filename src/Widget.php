<?php

namespace Phycom\Auth;

/**
 * Class Widget
 *
 * @package Phycom\Auth
 */
class Widget extends \yii\base\Widget
{
    public array $options = [];

    protected ?Module $module;

    public function init()
    {
        parent::init();
        $this->module = Yii::$app->getModule('auth');
    }

    /**
     * @return string
     */
    public function run()
    {
        $out = [];
        if ($this->module) {
            foreach ($this->module->getMethods() as $method) {
                $out[] = $method->render();
            }
        }
        return implode("\n", $out);
    }
}
