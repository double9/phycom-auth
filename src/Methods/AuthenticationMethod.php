<?php

namespace Phycom\Auth\Methods;

use Phycom\Auth\Models\Settings;

use Phycom\Base\Models\Traits\ModuleSettingsTrait;

use Phycom\Base\Modules\BaseModule;

/**
 * Class Module
 * @package Phycom\Auth\Methods
 */
abstract class AuthenticationMethod extends BaseModule
{
    use ModuleSettingsTrait;

    /**
     * @var bool
     */
    public bool $enabled = true;

    /**
     * @var bool
     */
    public bool $testMode = false;

    /**
     * @var bool
     */
    public bool $autoSignup = true;

    /**
     * @var string
     */
    public string $buttonClass = 'auth-method';

    /**
     * @return string
     */
    public function getId() : string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function getIsEnabled() : bool
    {
        return $this->enabled;
    }

    /**
     * @return string|array|callable
     */
    public function getSettingsForm()
    {
        return Settings::class;
    }

    /**
     * @var string|null $label
     * @var array $options
     * @return string
     */
    abstract public function render(string $label = null, array $options = []) : string;
}
