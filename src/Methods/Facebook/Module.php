<?php

namespace Phycom\Auth\Methods\Facebook;

use Phycom\Auth\Methods\AuthenticationMethod;
use Phycom\Auth\Methods\Facebook\Assets\FbAsset;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class Module
 * @package Phycom\Auth\Methods\Facebook
 */
class Module extends AuthenticationMethod
{
    /**
     * @var string
     */
    public $controllerNamespace = 'Phycom\Auth\Methods\Facebook\Controllers';

    /**
     * @var string
     */
    public $defaultRoute = 'signup';

    /**
     * @var string
     */
    public string $appId = '';

    /**
     * @param string|null $label
     * @param array $options
     * @return string
     */
    public function render(string $label = null, array $options = []) : string
    {
        $label = $label ?: Yii::t('phycom/auth','Facebook');
        $options = ArrayHelper::merge([
            'id'       => 'facebook-login-button',
            'class'    => 'btn btn-blue',
            'data-url' => Yii::$app->urlManager->createAbsoluteUrl(['/auth/facebook/signup/authenticate'])
        ], $options);

        Html::addCssClass($options, $this->buttonClass);

        FbAsset::register(Yii::$app->view);

        return Html::button($label, $options);
    }

    /**
     * @return string|null
     */
    public function getLogo(): ?string
    {
        $bundle = Assets\BrandAsset::register(Yii::$app->view);
        return $bundle->baseUrl . '/facebook.svg';
    }

    /**
     * @return array|string|callable
     */
    public function getSettingsForm()
    {
        return Settings::class;
    }
}
