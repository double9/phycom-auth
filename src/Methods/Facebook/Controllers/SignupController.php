<?php

namespace Phycom\Auth\Methods\Facebook\Controllers;


use Http\Discovery\Psr17FactoryDiscovery;
use Phycom\Auth\Methods\Facebook\Module;
use Phycom\Auth\Models\ExternalSignupForm;

use Phycom\Base\Models\UserTag;
use Phycom\Base\Models\Traits\WebControllerTrait;

use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\MessageFactoryDiscovery;

use yii\web\Controller as BaseController;
use yii\web\Response;
use yii\helpers\Json;
use Yii;

/**
 * Class FacebookController
 * @package frontend\controllers
 */
class SignupController extends BaseController
{
	use WebControllerTrait;

	public $enableCsrfValidation = false;

	public function actionIndex()
	{
		return $this->redirect(Yii::$app->urlManager->baseUrl . '/site/index');
	}

	public function actionAuthenticate()
	{
		Yii::info('Facebook authenticate - ' . json_encode(Yii::$app->request->post()), __METHOD__);
		$this->ajaxOnly(Response::FORMAT_JSON);

        $request = Psr17FactoryDiscovery::findRequestFactory()
            ->createRequest('GET', 'https://graph.facebook.com/me?fields=email,first_name,last_name&access_token=' . Yii::$app->request->post('token'));
		$httpAsyncClient = HttpClientDiscovery::find();

		$response = $httpAsyncClient->sendRequest($request);

        if ($response->getStatusCode() !== 200) {
            Yii::error('Invalid response status code ' . $response->getStatusCode(), __METHOD__);
            Yii::error('Response body: ' . $response->getBody()->getContents(), __METHOD__);
        }

		$data = Json::decode($response->getBody()->getContents());

		if (!isset($data['error'])) {
			$model = new ExternalSignupForm();
			$model->email = $data['email'];
			$model->firstName = $data['first_name'];
			$model->lastName = $data['last_name'];
			$model->externalId = $data['id'];

			$user = $model->getUser();

			if ($user === null) {
                if (!Module::getInstance()->autoSignup) {
                    FlashMsg::error(Yii::t('phycom/auth', 'User not found'));
                    return $this->goBack();
                }
				$user = $model->signup();
				$user->tag(UserTag::TAG_REGISTERED_WITH_FACEBOOK);
			}
			Yii::$app->getUser()->login($user);
		}
		return $this->goBack();
	}
}
