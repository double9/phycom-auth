<?php

namespace Phycom\Auth\Methods\Facebook\Assets;

use Phycom\Auth\Methods\Facebook\Module;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii;

/**
 * Fb login asset bundle.
 */
class FbAsset extends AssetBundle
{
	public $sourcePath = '@vendor/phycom/auth/src/Methods/Facebook/Assets/fb';
	public $js = [
		'facebook.js',
	];
	public $depends = [
		JqueryAsset::class,
	];

	public function init()
    {
        parent::init();
        $this->jsOptions = [
            'id' => 'fb-auth-script',
            'data-appid' => $this->getModule()->appId
        ];
    }

    /**
     * @return null|yii\base\Module|Module
     */
    public function getModule()
    {
        return Yii::$app->getModule('auth')->getModule('facebook');
    }
}
