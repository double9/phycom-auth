(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id))
		return;
	js = d.createElement(s);
	js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&cookie=1&version=v3.10&appId=" + d.getElementById('fb-auth-script').dataset.appid;
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

jQuery(function($) {
	var $btn = $("#facebook-login-button");
	$btn.on('click', function (e) {

		FB.getLoginStatus(function (response) {
			if (response.status === 'connected') {
                $.post($btn.data('url'), {token: response.authResponse.accessToken});
			} else {
                FB.login(function (login) {
                    if (login.status !== 'undefined') {
                        $.post($btn.data('url'), {token: login.authResponse.accessToken});
                    }
                }, {scope: 'public_profile,email'});
			}
		});

	});
	console.log("FB JS ready");
});