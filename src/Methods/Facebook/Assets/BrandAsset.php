<?php

namespace Phycom\Auth\Methods\Facebook\Assets;

use yii\web\AssetBundle;

/**
 * Facebook brand asset bundle.
 */
class BrandAsset extends AssetBundle
{
    public $sourcePath = '@vendor/phycom/auth/src/Methods/Facebook/Assets/brand';
}
