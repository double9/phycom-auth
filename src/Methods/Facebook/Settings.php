<?php

namespace Phycom\Auth\Methods\Facebook;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class Settings
 *
 * @package Phycom\Auth\Methods\Facebook
 */
class Settings extends \Phycom\Auth\Models\Settings
{
    public $appId;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['appId', 'string'];
        return $rules;
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'appId'  => Yii::t('phycom/auth', 'App ID'),
        ]);
    }

    public function attributeHints()
    {
        return ArrayHelper::merge(parent::attributeHints(), [
            'appId'  => Yii::t('phycom/auth', 'Facebook App ID used for authentication')
        ]);
    }
}
