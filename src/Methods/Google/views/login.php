<?php

use Phycom\Auth\Methods\Google\Assets\GoogleAsset;

$url = Yii::$app->urlManager->createAbsoluteUrl(['auth/google/signup/authenticate']);

$this->registerJs(
<<<JS
    jQuery(function ($) {
		$.post('$url', window.location.hash);
    });
JS
, yii\web\View::POS_END);

GoogleAsset::register($this);
?>
<div id="google-auth-loading">
    <div class="alert-container">
        <h3><?= Yii::t('phycom/auth', 'Authentication in progress...') ?></h3>
    </div>
</div>
