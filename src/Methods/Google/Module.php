<?php

namespace Phycom\Auth\Methods\Google;


use Phycom\Auth\Methods\AuthenticationMethod;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;

/**
 * Class Module
 * @package Phycom\Auth\Methods\Google
 */
class Module extends AuthenticationMethod
{
    /**
     * @var string
     */
    public $controllerNamespace = 'Phycom\Auth\Methods\Google\Controllers';
    /**
     * @var string
     */
    public $defaultRoute = 'signup';

    /**
     * @var string
     */
    public string $clientId = '';

    /**
     * @var array|string[]
     */
    public array $authScope = [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/user.phonenumbers.read',
        'https://www.googleapis.com/auth/user.addresses.read'
    ];

    /**
     * @var string
     */
    protected string $oAuthUrl = 'https://accounts.google.com/o/oauth2/auth';

    /**
     * @param string|null $label
     * @param array $options
     * @return string
     */
    public function render(string $label = null, array $options = []) : string
    {
        $label = $label ?: Yii::t('phycom/auth','Google');
        $options = ArrayHelper::merge([
            'id'    => 'google-login-button',
            'class' => 'btn btn-danger'
        ], $options);

        Html::addCssClass($options, $this->buttonClass);

        return Html::a($label, $this->createAuthUrl(), $options);
    }

    /**
     * @param string|null $redirect - url to redirect after authentication
     * @return string
     */
    public function createAuthUrl(string $redirect = null) : string
    {
        $redirect = $redirect ?: Yii::$app->urlManager->createAbsoluteUrl(['/auth/google/']);
        $url = $this->oAuthUrl .
            '?scope=' . implode(' ', $this->authScope) .
            '&client_id=' . $this->clientId .
            '&redirect_uri=' . $redirect .
            '&response_type=token';

        return $url;
    }

    /**
     * @return string|null
     */
    public function getLogo(): ?string
    {
        $bundle = Assets\BrandAsset::register(Yii::$app->view);
        return $bundle->baseUrl . '/google.svg';
    }

    /**
     * @return array|string|callable
     */
    public function getSettingsForm()
    {
        return Settings::class;
    }
}
