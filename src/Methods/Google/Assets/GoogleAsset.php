<?php

namespace Phycom\Auth\Methods\Google\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class GoogleAsset
 *
 * @package Phycom\Auth\Methods\Google\Assets
 */
class GoogleAsset extends AssetBundle
{
    public $sourcePath = '@vendor/phycom/auth/src/Methods/Google/Assets/google';
    public $css = [
        'login.css'
    ];
    public $js = [
    ];
    public $publishOptions = ['except' => ['*.less', '*.scss']];
    public $depends = [
        JqueryAsset::class
    ];
}
