<?php

namespace Phycom\Auth\Methods\Google\Assets;

use yii\web\AssetBundle;

/**
 * Google brand asset bundle.
 */
class BrandAsset extends AssetBundle
{
    public $sourcePath = '@vendor/phycom/auth/src/Methods/Google/Assets/brand';
}
