<?php

namespace Phycom\Auth\Methods\Google\Controllers;

use Phycom\Base\Helpers\FlashMsg;
use Phycom\Auth\Methods\google\Module;
use Phycom\Auth\Models\ExternalSignupForm;

use Phycom\Base\Models\UserTag;
use Phycom\Base\Models\Traits\WebControllerTrait;

use Http\Discovery\Psr17FactoryDiscovery;
use Http\Discovery\HttpClientDiscovery;

use yii\web\Controller as BaseController;
use yii\helpers\Json;
use yii\web\Response;
use Yii;

/**
 * Class GoogleController
 * @package Phycom\Auth\Methods\Google\Controllers
 */
class SignupController extends BaseController
{
	use WebControllerTrait;

	public $enableCsrfValidation = false;

	public function actionIndex()
	{
		return $this->render('/login');
	}

	public function actionAuthenticate()
	{
		Yii::info('Google authenticate - ' . json_encode(Yii::$app->request->post()), __METHOD__);
		$this->ajaxOnly(Response::FORMAT_JSON);

		$token = Yii::$app->request->post('#access_token');
		$tokenType = Yii::$app->request->post('token_type');

		$request = Psr17FactoryDiscovery::findRequestFactory()
            ->createRequest('GET', 'https://www.googleapis.com/oauth2/v3/userinfo')
            ->withHeader('Authorization', $tokenType . ' ' . $token);

		$httpAsyncClient = HttpClientDiscovery::find();

		$response = $httpAsyncClient->sendRequest($request);

		if ($response->getStatusCode() !== 200) {
		    Yii::error('Invalid response status code ' . $response->getStatusCode(), __METHOD__);
            Yii::error('Response body: ' . $response->getBody()->getContents(), __METHOD__);
        }

		$data = Json::decode($response->getBody()->getContents());

		if (!isset($data['error'])) {
			$model = new ExternalSignupForm();
			$model->email = $data['email'];
            $model->emailVerified = $data['email_verified'];
			$model->firstName = $data['given_name'];
			$model->lastName = $data['family_name'];
			$model->externalId = $data['sub'];

			$user = $model->getUser();

			if ($user === null) {
			    if (!Module::getInstance()->autoSignup) {
			        FlashMsg::error(Yii::t('phycom/auth', 'User not found'));
                    return $this->goBack();
                }
				$user = $model->signup();
				$user->tag(UserTag::TAG_REGISTERED_WITH_GOOGLE);
			}
			Yii::$app->getUser()->login($user);
		}
		return $this->goBack();
	}
}
