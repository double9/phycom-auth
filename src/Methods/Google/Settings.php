<?php

namespace Phycom\Auth\Methods\Google;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class Settings
 *
 * @package Phycom\Auth\Methods\Google
 */
class Settings extends \Phycom\Auth\Models\Settings
{
    public $clientId;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['clientId', 'string'];
        return $rules;
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'clientId'  => Yii::t('phycom/auth', 'Client ID'),
        ]);
    }

    public function attributeHints()
    {
        return ArrayHelper::merge(parent::attributeHints(), [
            'clientId'  => Yii::t('phycom/auth', 'Google authentication service client ID')
        ]);
    }
}
