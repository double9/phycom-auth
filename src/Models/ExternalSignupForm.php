<?php
namespace Phycom\Auth\Models;

use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Attributes\UserStatus;
use Phycom\Base\Models\Attributes\UserType;
use Phycom\Base\Models\Email;
use Phycom\Base\Models\User;

use yii\base\Model;
use Yii;


/**
 * Signup form
 */
class ExternalSignupForm extends Model
{
	use ModelTrait;

    public $email;
    public $firstName;
    public $lastName;
    public $externalId;

    public bool $emailVerified = true;

    private ?User $user = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstName','lastName', 'externalId'], 'trim'],
	        [['firstName','lastName', 'externalId'], 'required'],
            [['firstName','lastName', 'externalId'], 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
	        ['email', 'unique',
		        'targetClass' => Email::class,
		        'filter' => function ($query) {
			        /**
			         * @var yii\db\Query $query
			         */
			        return $query
				        ->andWhere('user_id IS NOT NULL')
				        ->andWhere(['not', ['status' => ContactAttributeStatus::DELETED]]);
	            },
		        'message' => 'This email address has already been taken.'
	        ],
            ['emailVerified', 'boolean']
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|false the saved model or false if saving fails
     * @throws \Exception
     */
    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
	        $user = Yii::$app->modelFactory->getUser();
	        $user->first_name = $this->firstName;
	        $user->last_name = $this->lastName;
	        $user->type = UserType::CLIENT;
	        $user->status = UserStatus::ACTIVE;
	        $user->generateAuthKey();

			if (!$user->save()) {
				return $this->rollback($transaction, $user->errors);
			}

	        $email = new Email();
	        $email->user_id = $user->id;
	        $email->status = $this->emailVerified
                ? ContactAttributeStatus::VERIFIED
                : ContactAttributeStatus::UNVERIFIED;
	        $email->email = $this->email;

	        if (!$email->save()) {
		        return $this->rollback($transaction, $email->errors);
	        }

	        $transaction->commit();
			return $user;

        } catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
    	if ($this->user === null) {
    		$this->user = Yii::$app->modelFactory->getUser()::findByEmail($this->email);
    	}
    	return $this->user;
    }
}
