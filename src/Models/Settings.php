<?php

namespace Phycom\Auth\Models;


use Phycom\Base\Models\ModuleSettingsForm;

use Phycom\Auth\Methods\AuthenticationMethod;
use Yii;

/**
 * Class Settings
 * @package Phycom\Auth\Models
 *
 * @property-read AuthenticationMethod $module
 */
class Settings extends ModuleSettingsForm
{
    public $enabled;
    public $testMode;

    public function rules()
    {
        return [
            ['enabled', 'boolean'],
            ['testMode', 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'enabled'  => Yii::t('phycom/auth', 'Enabled'),
            'testMode' => Yii::t('phycom/auth', 'Test Mode'),
        ];
    }

    public function attributeHints()
    {
        return [
            'enabled'  => Yii::t('phycom/auth', 'Is authentication method enabled'),
            'testMode' => Yii::t('phycom/auth', 'Execute on test environment'),
        ];
    }
}
